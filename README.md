**First server run with** "#.info.#"

**Second server run with** "#.error.#"

**Third server run with** "*#.warning.#"


First Client send "cpu.critical.info: Critical information about CPU". 

First Server catch it, insert data in database and send respons to Client: "[x] Data 'cpu.critical.info':'Critical information about CPU' successful inserted in rabbitMQ.data"

https://photos.app.goo.gl/isy9EHMoYXPFaLZq9


Second Client send "cpu.critical.error: Critical error CPU".

Second Server catch it, insert data in database and send respons to Client: "[x] Data 'cpu.critical.error':'Critical error CPU' successful inserted in rabbitMQ.data"

https://photos.app.goo.gl/KrAn1G1gTtxWAmKT9

And same for other.

**MongoDB result:**
https://photos.app.goo.gl/Y3Aq2hY75t2veNNW6
