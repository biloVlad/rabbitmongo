
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.bson.Document;

public class RPCServer {

    private static final String EXCHANGE_NAME = "topic_mongo";
    private static final String RPC_QUEUE_NAME = "rpc_queue";
    private final static String NAME_DATABASE = "rabbitMQ";
    private final static String COLLECTION = "data";

    private static int fib(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public static void main(String[] argv) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        MongoClient mongoClient = new MongoClient();
        MongoDatabase database = mongoClient.getDatabase(NAME_DATABASE);

        Connection connection = null;
        try {
            connection = factory.newConnection();
            final Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
            String queueName = channel.queueDeclare().getQueue();

            if (argv.length < 1) {
                System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
                System.exit(1);
            }

            for (String bindingKey : argv) {
                channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);
            }

            //channel.queueDeclare(queueName, false, false, false, null);
            channel.basicQos(1);

            System.out.println(" [x] Awaiting RPC requests");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();

                    String response = "";

                    try {
                        String message = new String(body, "UTF-8");
                        System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");

                        Document doc = new Document("routingKey", envelope.getRoutingKey())
                                .append("value", message);
                        MongoCollection<Document> collection = database.getCollection(COLLECTION);
                        collection.insertOne(doc);

                        response = " [x] Data '" + envelope.getRoutingKey() + "':'" + message + "' successful inserted in " + NAME_DATABASE + "." + COLLECTION;
                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e.toString());
                    } finally {

                        channel.basicPublish("", properties.getReplyTo(), replyProps, response.getBytes("UTF-8"));
                        channel.basicAck(envelope.getDeliveryTag(), false);
                        // RabbitMq consumer worker thread notifies the RPC server owner thread 
                        synchronized (this) {
                            this.notify();
                        }
                    }
                }
            };

            channel.basicConsume(queueName, false, consumer);
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException _ignore) {
                }
            }
        }
    }
}
